from django.db import models

class LocationMarket(models.Model):
    id_type_market = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=30)
    ubicacion = models.CharField(max_length=120) 
    encargado = models.CharField(max_length=120, null=True, blank=True, default=None)
    observaciones = models.TextField(blank=True, null=True, blank=True,  default=None)
    date_create = models.DateTimeField(auto_now=True, null=True, default=None)
    last_update = models.DateTimeField(auto_now=True, null=True, default=None)

    class Meta:
        verbose_name = 'Ubicación'
        verbose_plural_name = 'Ubicaciones'