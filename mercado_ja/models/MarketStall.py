from django.db import models

from mercado_ja.models import Category, LocationMarket, Owner, TypeMrket


class MarketStall(models.Model):
    '''
    Representa un local comercial    
    '''    
    id_market_sall = models.AutoField(primary_key=True)
    id_owner = models.ForeignKey(Owner, db_column='id_owner', on_delete=models.PROTECT)
    id_category = models.ForeignKey(Category, db_column='id_category', on_delete=models.PROTECT)
    id_location = models.ForeignKey(LocationMarket, db_column='id_location', on_delete=models.PROTECT)
    identificacion = models.CharField(max_lenght=30, blank=True, null=True)
    tipo = models.CharField(max_lenght=30)