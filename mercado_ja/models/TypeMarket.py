from django.db import models

class TypeMarket(models.Model):
    id_type_market = models.AutoField(primary_key=True)
    tipo_negocio = models.CharField(max_length=40, unique=True)
    detalle = models.TextField(blank=True, null=True, default=None)
    date_create = models.DateTimeField(auto_now=True)
    last_update = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Tipos de Negocios'
        verbose_plural_name = 'Tipo de Negocio'
        ordering = ['tipo_negocio']

    
