from django.db import models
from django.contrib.auth.models import User

class Owner(models.Model):
    ''' 
    Representa a la persona duena de un puesto de mercado
    '''
    id_owner = models.AutoField(primary_key=True)
    nombres = models.CharField(max_length=50)
    apellidos = models.CharField(max_length=50)
    nro_cedula = models.CharField(max_length=10)
    correo = models.EmailField(max_length=50,null=True, blank=True, default=None)
    fecha_nacimiento = models.DateField(blank=True, default=None)
    id_user = models.ForeignKey(User, db_column='id_user', on_delete=models.PROTECT)
    date_create = models.DateTimeField(auto_now=True, blank=True, default=None)
    last_update = models.DateTimeField(blank=True, default=None)

    
    class Meta:
        verbose_name = 'Propietario'
        verbose_name_plural='Propietarios'
