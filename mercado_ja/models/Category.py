from django.db import models

class Category(models.Model):
    id_category = models.AutoField(primary_key=True)
    categoria = models.CharField(unique=True, max_legth=25)
    descripcion = models.TextField(blank=True, null=True, default=None)
    date_create = models.DateTimeField(auto_now=True, blank=True, default=None)
    last_update = models.DateTimeField(blank=True, default=None)

    class Meta:
        vebrose_name_plural = 'Categorias Puestos'
        verbose_name = 'Categoría'
        ordering = ['categoria']